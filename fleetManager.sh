#!/bin/bash

# checking all Active Fleets
#Get all the regions that are running
sleep 600
REGIONS=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?filterByFormula=NOT(%7BRun%7D+%3D+'')" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq '.records[]' | jq -r '.fields.zone')
for REGION in ${REGIONS[@]}
do
  ACTIVE_FLEETS=$(aws ec2 describe-spot-fleet-requests --query "SpotFleetRequestConfigs[?SpotFleetRequestState == 'active' ]" --region ${REGION} | jq -r '.[]' | jq -r '.SpotFleetRequestId')
  for FLEET in ${ACTIVE_FLEETS[@]}
  do

    #get all Fleets
    FLEET_INFO=$(aws ec2 describe-spot-fleet-requests --region ${REGION} --spot-fleet-request-id ${FLEET} | jq -r '.')
    CREATE_TIME=$(jq -n "$FLEET_INFO" | jq -r .SpotFleetRequestConfigs |  jq -r '.[]' |  jq -r '.CreateTime')
    LIMIT=$(jq -n "$FLEET_INFO" | jq -r .SpotFleetRequestConfigs |  jq -r '.[]' |  jq -r '.SpotFleetRequestConfig' |  jq -r '.TargetCapacity')
    FULFILLED=$(jq -n "$FLEET_INFO" | jq -r .SpotFleetRequestConfigs |  jq -r '.[]' |  jq -r '.SpotFleetRequestConfig' |  jq -r '.FulfilledCapacity')

    START_TIME=$(date -d $CREATE_TIME +"%s")
    FLEET_AGE=$(echo "$(date +"%s") - ($START_TIME)" | bc -l | awk '{printf "%.0f", $0}' )

    if [ $FLEET_AGE -gt 10800 ]; then
      aws ec2 cancel-spot-fleet-requests --region ${REGION} --spot-fleet-request-ids ${FLEET} --no-terminate-instances
    fi
  done

done
