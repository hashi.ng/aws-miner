#!/bin/bash

chmod +x configureAWS.sh
./configureAWS.sh

REGIONS=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?filterByFormula=NOT(%7BRun%7D+%3D+'')" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq '.records[]' | jq -r '.fields.zone')
for REGION in ${REGIONS[@]}
do
  ACTIVE_FLEETS=$(aws ec2 describe-spot-fleet-requests --query "SpotFleetRequestConfigs[?SpotFleetRequestState == 'active' ]" --region ${REGION} | jq -r '.[]' | jq -r '.SpotFleetRequestId')
  for FLEET in ${ACTIVE_FLEETS[@]}
  do
    aws ec2 cancel-spot-fleet-requests --region ${REGION} --spot-fleet-request-ids ${FLEET} --terminate-instances
  done
done
