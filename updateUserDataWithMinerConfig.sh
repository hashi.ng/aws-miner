#!/bin/bash

# get user-data
COIN=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?fields%5B%5D=coin&filterByFormula=%7Bzone%7D%3D%22${X}%22" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq -r '.records[]' | jq -r '.fields.coin')
MINERCONFIG=https://gitlab.com/hashi.ng/miner-configs/raw/master/$COIN-config.json
#MINERCONFIG=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?fields%5B%5D=miner-config-json&filterByFormula=%7Bzone%7D%3D%22${X}%22" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq -r '.records[]' | jq -r  '.fields[]')
#echo "$MINERCONFIG" > /root/config.json

sed \
-e "s@MinerConfig@$MINERCONFIG@" \
-e "s/coin/$COIN/" \
/root/aws-miner/user-data.txt.tmp > /root/${X}-user-data.txt
