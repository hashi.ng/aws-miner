#!/bin/bash

# get config.json
TARGETCAPACITY=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?fields%5B%5D=limit&filterByFormula=%7Bzone%7D%3D%22${X}%22" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq -r '.records[]' | jq -r '.fields.limit')
SPOTPRICE=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?fields%5B%5D=instance_cost&filterByFormula=%7Bzone%7D%3D%22${X}%22" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq -r '.records[]' | jq -r '.fields.instance_cost')
VALIDFROM=$(date +%Y-%m-%dT%H:%M:%S)
VALIDUNTIL=$(date -d "1  year" +%Y-%m-%dT%H:%M:%S)
INSTANCETYPE=$(curl "https://api.airtable.com/v0/appRF35xHzyRZR7Bl/tbltP8P0iS10ovKMu?fields%5B%5D=instance_type&filterByFormula=%7Bzone%7D%3D%22${X}%22" -H "Authorization: Bearer keygD9Z2v7ErMlYjA" | jq -r '.records[]' | jq -r '.fields.instance_type')
USERDATA=$(cat /root/${X}-user-data.txt | base64 -w 0)


sed \
-e "s/\${TargetCapacity}/$TARGETCAPACITY/" \
-e "s/\${SpotPrice}/$SPOTPRICE/" \
-e "s/\${ValidFrom}/$VALIDFROM/" \
-e "s/\${ValidUntil}/$VALIDUNTIL/" \
-e "s/\${InstanceType}/$INSTANCETYPE/" \
-e "s/\${UserData}/$USERDATA/" \
/root/aws-miner/instance-templates/${X}-config.json.tmp > /root/${X}-config.json
